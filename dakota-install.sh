#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace

samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)

dakota-install() {
  local platform=$1
  local repo_dir=$PWD/tmp/$platform/dakota

  if ! test -d $repo_dir; then
    git clone ${repo:-git@gitlab.com:robertnielsen/dakota.git} $repo_dir
  fi
  cd $repo_dir
  git checkout ${branch:-staging}
  if test ${extra:-0} -eq 0; then
    ./configure-$platform
  else
    ./configure-$platform-extra
  fi
  ./f -s
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  dakota-install "$@"
fi
