#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
#set -o xtrace

samedir=$(cd "$(dirname "$BASH_SOURCE")" && pwd)

# if we used 'getconf _NPROCESSORS_ONLN' on darwin/macos
# it would report num-of-cores * num-threads-per-core
# which is really incorrect
core-count() {
  if test "$(uname)" == Darwin; then
    sysctl -n hw.physicalcpu
  else
    getconf _NPROCESSORS_ONLN
  fi
}
dakota-test() {
  local platform=$1
  local source_dir=$PWD/tmp/$platform/dakota/test
  cd $source_dir
  git checkout ${branch:-staging}
  ./configure
  local core_count; core_count=$(core-count)
  source_dir=$source_dir make       --jobs $core_count
  source_dir=$source_dir make check --jobs $core_count
}
if test "$0" == "${BASH_SOURCE:-$0}"; then
  dakota-test "$@"
fi
